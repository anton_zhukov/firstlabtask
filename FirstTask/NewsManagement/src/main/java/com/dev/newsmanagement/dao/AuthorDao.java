package com.dev.newsmanagement.dao;

import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Author;
import com.dev.newsmanagement.entity.News;

import java.util.List;

public interface AuthorDao extends BasicDao<Author> {

    /**
     * Bind author with added news
     *
     * @param newsId
     * @param author
     * @throws DaoException
     */
    void addAuthorToNews(long newsId, Author author) throws DaoException;

    /**
     * Gets author of news by its id
     *
     * @param newsId
     * @return author, got by news id
     * @throws DaoException
     */
    Author getNewsAuthorByNewsId(long newsId) throws DaoException;

    /**
     * Gets list of authors
     *
     * @return list of all authors
     * @throws DaoException
     */
    List<Author> getAllAuthors() throws DaoException;

    /**
     * Changes news' author
     *
     * @param modifiedAuthor
     * @param news
     * @throws DaoException
     */
    void editNewsAuthor(Author modifiedAuthor, News news) throws DaoException;

    /**
     * Removes author from definite news
     *
     * @param newsIds
     * @throws DaoException
     */
    void deleteAuthorFromNews(long... newsIds) throws DaoException;

    /**
     * Makes author expired
     *
     * @param authorId
     * @throws DaoException
     */
    void makeAuthorExpired(long authorId) throws DaoException;
}