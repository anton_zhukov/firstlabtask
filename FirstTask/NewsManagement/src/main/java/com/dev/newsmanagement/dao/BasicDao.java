package com.dev.newsmanagement.dao;

import com.dev.newsmanagement.exception.DaoException;

/**
 * Generic interface with four basic C.R.U.D operations
 *
 * @param <T>
 */
public interface BasicDao<T> {

    /**
     * Adds row to database
     *
     * @param t
     * @return
     * @throws DaoException
     */
    long add(T t) throws DaoException;

    /**
     * Gets row from database
     *
     * @param id
     * @return
     * @throws DaoException
     */
    T get(long id) throws DaoException;

    /**
     * Edits row in database
     *
     * @param t
     * @throws DaoException
     */
    void edit(T t) throws DaoException;

    /**
     * Deletes row in database
     *
     * @param id
     * @throws DaoException
     */
    void delete(long id) throws DaoException;
}