package com.dev.newsmanagement.dao;

import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Comment;

import java.util.List;

public interface CommentsDao extends BasicDao<Comment> {

    /**
     * Gets list of comments by definite news id
     *
     * @param newsId
     * @return list of comments, got by news id
     * @throws DaoException
     */
    List<Comment> getCommentsByNewsId(long newsId) throws DaoException;

    /**
     * Delete comments from various number of news
     *
     * @param newsIds
     * @throws DaoException
     */
    void deleteCommentsFromNews(long... newsIds) throws DaoException;

    /**
     * Counts comments by news id
     *
     * @param newsId
     * @return number of comments in single news
     * @throws DaoException
     */
    long countAllCommentsByNewsId(long newsId) throws DaoException;
}