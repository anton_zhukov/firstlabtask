package com.dev.newsmanagement.dao;

import com.dev.newsmanagement.entity.Tag;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.News;
import com.dev.newsmanagement.entity.SearchEntity;

import java.util.List;

public interface NewsDao extends BasicDao<News> {

    /**
     * Gets list of news ordered by number of comments
     *
     * @return list of all news
     * @throws DaoException
     */
    List<News> getAllNews() throws DaoException;

    /**
     * Gets news list found by tag ids
     *
     * @param tags
     * @return list of news got by tagIds
     * @throws DaoException
     */
    List<News> getNewsByTagsIds(List<Tag> tags) throws DaoException;

    /**
     * Gets news list found by author id
     *
     * @param authorId
     * @return list of news got by authorId
     * @throws DaoException
     */
    List<News> getNewsByAuthorId(long authorId) throws DaoException;

    /**
     * Gets news list found by tag ids and author id
     *
     * @param searchEntity
     * @return list of news got by authorId and tagIds
     * @throws DaoException
     */
    List<News> getNewsByTagIdsAndAuthorId(SearchEntity searchEntity) throws DaoException;

    /**
     * Deletes various number of news by news ids
     *
     * @param newsIds
     * @throws DaoException
     */
    void deleteNews(long... newsIds) throws DaoException;

    /**
     * Counts number of news
     *
     * @return number of news
     * @throws DaoException
     */
    long countAllNews() throws DaoException;
}