package com.dev.newsmanagement.dao;

import com.dev.newsmanagement.entity.Role;

public interface RoleDao extends BasicDao<Role> {

    
}