package com.dev.newsmanagement.dao;

import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Tag;

import java.util.List;

public interface TagDao extends BasicDao<Tag> {

    /**
     * Bind tags with news
     *
     * @param newsId
     * @param tags
     * @throws DaoException
     */
    void addTagsToNews(long newsId, List<Tag> tags) throws DaoException;

    /**
     * Gets list of tags from definite news
     *
     * @param newsId
     * @return list of tags got by newsId
     * @throws DaoException
     */
    List<Tag> getNewsTagsByNewsId(long newsId) throws DaoException;

    /**
     * Gets list of all tags
     *
     * @return list of all tags
     * @throws DaoException
     */
    List<Tag> getAllTags() throws DaoException;

    /**
     * Unbind tag from all news that contains it
     *
     * @param tagId
     * @throws DaoException
     */
    void deleteTagFromAllNews(long tagId) throws DaoException;

    /**
     * Unbind tags from all news that are deleting
     *
     * @param newsIds
     * @throws DaoException
     */
    void deleteTagsFromNewsByIds(long... newsIds) throws DaoException;
}