package com.dev.newsmanagement.dao;

import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.User;

public interface UsersDao extends BasicDao<User> {

    /**
     * Checks whether login exists
     *
     * @param login
     * @return true if login exists, false otherwise
     * @throws DaoException
     */
    boolean doesLoginExist(String login) throws DaoException;

    /**
     * Checks whether combination of login and password exists
     *
     * @param login
     * @param password
     * @return true if combination of login and password exists, false otherwise
     * @throws DaoException
     */
    boolean doesUserExist(String login, String password) throws DaoException;
}