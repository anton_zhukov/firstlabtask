package com.dev.newsmanagement.dao.impl;

import com.dev.newsmanagement.dao.AuthorDao;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Author;
import com.dev.newsmanagement.entity.News;
import com.dev.newsmanagement.util.TimeDateGenerator;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AuthorDaoImpl extends CommonDao implements AuthorDao {

    private static final String AUTHOR_ID = "AUTH_AUTHOR_ID";
    private static final String AUTHOR_NAME = "AUTH_AUTHOR_NAME";
    private static final String EXPIRED = "AUTH_EXPIRED";
    private static final String[] COLUMN_NAMES = new String[]{AUTHOR_ID};

    private static final String SQL_ADD_AUTHOR = "INSERT INTO AUTHORS (AUTH_AUTHOR_ID, AUTH_AUTHOR_NAME) " +
            "VALUES(AUTH_SEQ.NEXTVAL, ?)";
    private static final String SQL_ADD_AUTHOR_TO_NEWS = "INSERT INTO NEWS_AUTHORS (NAUT_NEWS_ID, NAUT_AUTHOR_ID) " +
            "VALUES(?, ?)";
    private static final String SQL_GET_ALL_AUTHORS = "SELECT AUTHORS.AUTH_AUTHOR_ID, AUTHORS.AUTH_AUTHOR_NAME, " +
            "AUTHORS.AUTH_EXPIRED FROM AUTHORS WHERE AUTHORS.AUTH_EXPIRED IS NULL";
    private static final String SQL_GET_AUTHOR_BY_ID = "SELECT AUTHORS.AUTH_AUTHOR_ID, AUTHORS.AUTH_AUTHOR_NAME, AUTHORS.AUTH_EXPIRED " +
            "FROM AUTHORS WHERE AUTHORS.AUTH_AUTHOR_ID=?";
    private static final String SQL_GET_NEWS_AUTHOR_BY_NEWS_ID = "SELECT AUTHORS.AUTH_AUTHOR_ID, AUTHORS.AUTH_AUTHOR_NAME, " +
            "AUTHORS.AUTH_EXPIRED FROM AUTHORS INNER JOIN NEWS_AUTHORS ON AUTHORS.AUTH_AUTHOR_ID=NEWS_AUTHORS.NAUT_AUTHOR_ID " +
            "WHERE NEWS_AUTHORS.NAUT_NEWS_ID=?";
    private static final String SQL_EDIT_AUTHOR_NAME = "UPDATE AUTHORS SET AUTHORS.AUTH_AUTHOR_NAME=? WHERE AUTHORS.AUTH_AUTHOR_ID=?";
    private static final String SQL_EDIT_NEWS_AUTHOR = "UPDATE NEWS_AUTHORS SET NEWS_AUTHORS.NAUT_AUTHOR_ID=? " +
            "WHERE NEWS_AUTHORS.NAUT_NEWS_ID=?";
    private static final String SQL_DELETE_AUTHOR_FROM_NEWS_PREFIX = "DELETE FROM NEWS_AUTHORS WHERE NEWS_AUTHORS.NAUT_NEWS_ID IN(";
    private static final String SQL_DELETE_AUTHOR_FROM_NEWS_POSTFIX = ")";
    private static final String SQL_DELETE_AUTHOR_BY_ID = "DELETE FROM AUTHORS WHERE AUTHORS.AUTH_AUTHOR_ID=?";
    private static final String SQL_MAKE_AUTHOR_EXPIRED = "UPDATE AUTHORS SET AUTHORS.AUTH_EXPIRED=? WHERE " +
            "AUTHORS.AUTH_AUTHOR_ID=?";

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#add(Object)
     */
    @Override
    public long add(Author author) throws DaoException {
        long authorId = 0;
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_ADD_AUTHOR, COLUMN_NAMES);
            ps.setString(1, author.getAuthorName());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            while (rs.next()) {
                authorId = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by adding author", e);
        } finally {
            closeResources(connection, ps);
        }
        return authorId;
    }

    /**
     * @see com.dev.newsmanagement.dao.AuthorDao#addAuthorToNews(long, com.dev.newsmanagement.entity.Author)
     */
    @Override
    public void addAuthorToNews(long newsId, Author author) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_ADD_AUTHOR_TO_NEWS);
            ps.setLong(1, newsId);
            ps.setLong(2, author.getAuthorId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by adding author to news", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#get(long)
     */
    @Override
    public Author get(long authorId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        Author author = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_GET_AUTHOR_BY_ID);
            ps.setLong(1, authorId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                author = setupAuthorFields(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting author by id");
        } finally {
            closeResources(connection, ps);
        }
        return author;
    }

    /**
     * @see com.dev.newsmanagement.dao.AuthorDao#getNewsAuthorByNewsId(long)
     */
    @Override
    public Author getNewsAuthorByNewsId(long newsId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        Author author = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_GET_NEWS_AUTHOR_BY_NEWS_ID);
            ps.setLong(1, newsId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                author = setupAuthorFields(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting news author by news id");
        } finally {
            closeResources(connection, ps);
        }
        return author;
    }

    /**
     * @see com.dev.newsmanagement.dao.AuthorDao#getAllAuthors()
     */
    @Override
    public List<Author> getAllAuthors() throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        List<Author> authors = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_GET_ALL_AUTHORS);
            ResultSet rs = ps.executeQuery();
            authors = new ArrayList<>();
            while (rs.next()) {
                authors.add(setupAuthorFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting all authors", e);
        } finally {
            closeResources(connection, ps);
        }
        return authors;
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#edit(Object)
     */
    @Override
    public void edit(Author modifiedAuthor) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_EDIT_AUTHOR_NAME);
            ps.setString(1, modifiedAuthor.getAuthorName());
            ps.setLong(2, modifiedAuthor.getAuthorId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by editing author", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.AuthorDao#editNewsAuthor(com.dev.newsmanagement.entity.Author, com.dev.newsmanagement.entity.News)
     */
    @Override
    public void editNewsAuthor(Author modifiedAuthor, News news) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_EDIT_NEWS_AUTHOR);
            ps.setLong(1, modifiedAuthor.getAuthorId());
            ps.setLong(2, news.getNewsId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by editing author in news", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#delete(long)
     */
    public void delete(long authorId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_DELETE_AUTHOR_BY_ID);
            ps.setLong(1, authorId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by deleting author", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.AuthorDao#deleteAuthorFromNews(long...)
     */
    public void deleteAuthorFromNews(long... newsIds) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(getResultQuery(SQL_DELETE_AUTHOR_FROM_NEWS_PREFIX, SQL_DELETE_AUTHOR_FROM_NEWS_POSTFIX,
                    newsIds.length));
            for (int i = 1; i <= newsIds.length; i++) {
                ps.setLong(i, newsIds[i - 1]);
            }
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by deleting author from news", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.AuthorDao#makeAuthorExpired(long)
     */
    @Override
    public void makeAuthorExpired(long authorId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_MAKE_AUTHOR_EXPIRED);
            ps.setTimestamp(1, TimeDateGenerator.getCurrentTimestamp());
            ps.setLong(2, authorId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by making author expired", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * Sets up fields of author instance
     *
     * @param rs
     * @return author instance with filled fields
     * @throws SQLException
     */
    private Author setupAuthorFields(ResultSet rs) throws SQLException {
        return new Author(rs.getLong(AUTHOR_ID), rs.getString(AUTHOR_NAME), rs.getTimestamp(EXPIRED));
    }
}