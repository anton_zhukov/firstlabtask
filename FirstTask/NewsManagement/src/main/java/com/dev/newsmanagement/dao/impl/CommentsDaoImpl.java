package com.dev.newsmanagement.dao.impl;

import com.dev.newsmanagement.dao.CommentsDao;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Comment;
import com.dev.newsmanagement.util.TimeDateGenerator;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentsDaoImpl extends CommonDao implements CommentsDao {

    private static final String COMMENT_ID = "COMT_COMMENT_ID";
    private static final String NEWS_ID = "COMT_NEWS_ID";
    private static final String COMMENT_TEXT = "COMT_COMMENT_TEXT";
    private static final String CREATION_DATE = "COMT_CREATION_DATE";

    private static final String[] COLUMN_NAMES = new String[]{COMMENT_ID};

    private static final String SQL_ADD_COMMENT = "INSERT INTO COMMENTS (COMT_COMMENT_ID, COMT_NEWS_ID, COMT_COMMENT_TEXT, " +
            "COMT_CREATION_DATE) VALUES (COMT_SEQ.NEXTVAL, ?, ?, ?)";
    private static final String SQL_GET_COMMENT_BY_COMMENT_ID = "SELECT COMMENTS.COMT_COMMENT_ID, COMMENTS.COMT_NEWS_ID, " +
            "COMMENTS.COMT_COMMENT_TEXT, COMMENTS.COMT_CREATION_DATE FROM COMMENTS WHERE COMMENTS.COMT_COMMENT_ID=?";
    private static final String SQL_GET_COMMENTS_BY_NEWS_ID = "SELECT COMMENTS.COMT_COMMENT_ID, COMMENTS.COMT_NEWS_ID, " +
            "COMMENTS.COMT_COMMENT_TEXT, COMMENTS.COMT_CREATION_DATE FROM COMMENTS WHERE COMMENTS.COMT_NEWS_ID=?";
    private static final String SQL_EDIT_COMMENT_TEXT = "UPDATE COMMENTS SET COMMENTS.COMT_COMMENT_TEXT=? WHERE " +
            "COMMENTS.COMT_COMMENT_ID=?";
    private static final String SQL_DELETE_COMMENT = "DELETE FROM COMMENTS WHERE COMMENTS.COMT_COMMENT_ID=?";
    private static final String SQL_DELETE_COMMENTS_FROM_NEWS_PREFIX = "DELETE FROM COMMENTS WHERE COMMENTS.COMT_NEWS_ID IN(";
    private static final String SQL_DELETE_COMMENTS_FROM_NEWS_POSTFIX = ")";
    private static final String SQL_COUNT_ALL_COMMENTS_BY_NEWS_ID = "SELECT COUNT(COMMENTS.COMT_COMMENT_ID) FROM COMMENTS WHERE " +
            "COMMENTS.COMT_NEWS_ID=?";

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#add(Object)
     */
    @Override
    public long add(Comment comment) throws DaoException {
        long commentId = 0;
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_ADD_COMMENT, COLUMN_NAMES);
            ps.setLong(1, comment.getNewsId());
            ps.setString(2, comment.getCommentText());
            ps.setTimestamp(3, TimeDateGenerator.getCurrentTimestamp());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            while (rs.next()) {
                commentId = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by adding comment", e);
        } finally {
            closeResources(connection, ps);
        }
        return commentId;
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#get(long)
     */
    @Override
    public Comment get(long commentId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        Comment comment = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_GET_COMMENT_BY_COMMENT_ID);
            ps.setLong(1, commentId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                comment = setupCommentFields(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting comment by comment id", e);
        } finally {
            closeResources(connection, ps);
        }
        return comment;
    }

    /**
     * @see com.dev.newsmanagement.dao.CommentsDao#getCommentsByNewsId(long)
     */
    @Override
    public List<Comment> getCommentsByNewsId(long newsId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        List<Comment> comments = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_GET_COMMENTS_BY_NEWS_ID);
            ps.setLong(1, newsId);
            ResultSet rs = ps.executeQuery();
            comments = new ArrayList<>();
            while (rs.next()) {
                comments.add(setupCommentFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting comments by news id", e);
        } finally {
            closeResources(connection, ps);
        }
        return comments;
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#edit(Object)
     */
    @Override
    public void edit(Comment modifiedComment) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_EDIT_COMMENT_TEXT);
            ps.setString(1, modifiedComment.getCommentText());
            ps.setLong(2, modifiedComment.getCommentId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by editing comment", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#delete(long)
     */
    @Override
    public void delete(long commentId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_DELETE_COMMENT);
            ps.setLong(1, commentId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by deleting comment", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.CommentsDao#deleteCommentsFromNews(long...)
     */
    public void deleteCommentsFromNews(long... newsIds) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(getResultQuery(SQL_DELETE_COMMENTS_FROM_NEWS_PREFIX, SQL_DELETE_COMMENTS_FROM_NEWS_POSTFIX,
                    newsIds.length));
            for (int i = 1; i <= newsIds.length; i++) {
                ps.setLong(i, newsIds[i - 1]);
            }
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by deleting comments from news", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.CommentsDao#countAllCommentsByNewsId(long)
     */
    @Override
    public long countAllCommentsByNewsId(long newsId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        long commentsQuantity = 0;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_COUNT_ALL_COMMENTS_BY_NEWS_ID);
            ps.setLong(1, newsId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                commentsQuantity = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by counting comments quantity by news id", e);
        } finally {
            closeResources(connection, ps);
        }
        return commentsQuantity;
    }

    /**
     * Sets up fields of comment instance
     *
     * @param rs
     * @return comment instance with filled fields
     * @throws SQLException
     */
    private Comment setupCommentFields(ResultSet rs) throws SQLException {
        Comment comment = new Comment();
        comment.setCommentId(rs.getLong(COMMENT_ID));
        comment.setNewsId(rs.getLong(NEWS_ID));
        comment.setCommentText(rs.getString(COMMENT_TEXT));
        comment.setCreationDate(rs.getTimestamp(CREATION_DATE));
        return comment;
    }
}