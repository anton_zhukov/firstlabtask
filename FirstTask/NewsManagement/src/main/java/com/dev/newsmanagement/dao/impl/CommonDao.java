package com.dev.newsmanagement.dao.impl;

import com.dev.newsmanagement.entity.Tag;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public abstract class CommonDao {

    private static final Logger LOG = Logger.getLogger(CommonDao.class);

    private static final String QUESTION_MARK = "?";
    private static final String COMMA = ",";

    @Autowired
    protected DataSource dataSource;

    /**
     * Closes statements and releases connections
     *
     * @param connection
     * @param statements
     */
    protected void closeResources(Connection connection, PreparedStatement... statements) {
        try {
            for (PreparedStatement statement : statements) {
                if (statement != null) {
                    statement.close();
                }
            }
        } catch (SQLException e) {
            LOG.error("Error by closing resources", e);
        } finally {
            if (connection != null) {
                DataSourceUtils.releaseConnection(connection, dataSource);
            }
        }
    }

    /**
     * Builds query with prefix and postfix parts
     *
     * @param prefix         query prefix
     * @param postfix        query postfix
     * @param elementsLength number of elements for sql operator IN()
     * @return result sql query
     */
    protected String getResultQuery(String prefix, String postfix, int elementsLength) {
        StringBuilder query = new StringBuilder(prefix);
        for (int i = 0; i < elementsLength; i++) {
            query.append(QUESTION_MARK);
            if (i != elementsLength - 1) {
                query.append(COMMA);
            }
        }
        query.append(postfix);
        return query.toString();
    }

    /**
     * Gets array of tag ids from list of tags
     *
     * @param tags
     * @return array of tagIds
     */
    protected long[] getTagIds(List<Tag> tags) {
        long[] tagIds = new long[tags.size()];
        for (int i = 0; i < tags.size(); i++) {
            tagIds[i] = tags.get(i).getTagId();
        }
        return tagIds;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}