package com.dev.newsmanagement.dao.impl;

import com.dev.newsmanagement.dao.NewsDao;
import com.dev.newsmanagement.entity.Tag;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.News;
import com.dev.newsmanagement.entity.SearchEntity;
import com.dev.newsmanagement.util.TimeDateGenerator;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NewsDaoImpl extends CommonDao implements NewsDao {

    private static final String NEWS_ID = "NEWS_NEWS_ID";
    private static final String TITLE = "NEWS_TITLE";
    private static final String SHORT_TEXT = "NEWS_SHORT_TEXT";
    private static final String FULL_TEXT = "NEWS_FULL_TEXT";
    private static final String CREATION_DATE = "NEWS_CREATION_DATE";
    private static final String MODIFICATION_DATE = "NEWS_MODIFICATION_DATE";

    private static final String[] COLUMN_NAMES = new String[]{NEWS_ID};

    private static final String SQL_ADD_NEWS = "INSERT INTO NEWS (NEWS_NEWS_ID, NEWS_TITLE, NEWS_SHORT_TEXT, NEWS_FULL_TEXT, " +
            "NEWS_CREATION_DATE, NEWS_MODIFICATION_DATE) VALUES (NEWS_SEQ.NEXTVAL, ?, ?, ?, ?, ?)";
    private static final String SQL_GET_ALL_NEWS = "SELECT NEWS.NEWS_NEWS_ID, NEWS.NEWS_TITLE, NEWS.NEWS_SHORT_TEXT, " +
            "NEWS.NEWS_FULL_TEXT, NEWS.NEWS_CREATION_DATE, NEWS.NEWS_MODIFICATION_DATE FROM NEWS ORDER BY (SELECT COUNT(1) " +
            "FROM COMMENTS WHERE COMMENTS.COMT_NEWS_ID=NEWS.NEWS_NEWS_ID) DESC";
    private static final String SQL_GET_NEWS_BY_NEWS_ID = "SELECT NEWS.NEWS_NEWS_ID, NEWS.NEWS_TITLE, NEWS.NEWS_SHORT_TEXT, " +
            "NEWS.NEWS_FULL_TEXT, NEWS.NEWS_CREATION_DATE, NEWS.NEWS_MODIFICATION_DATE FROM NEWS WHERE NEWS.NEWS_NEWS_ID=?";
    private static final String SQL_GET_NEWS_BY_AUTHOR_ID = "SELECT NEWS.NEWS_NEWS_ID, NEWS.NEWS_TITLE, NEWS.NEWS_SHORT_TEXT, " +
            "NEWS.NEWS_FULL_TEXT, NEWS.NEWS_CREATION_DATE, NEWS.NEWS_MODIFICATION_DATE FROM NEWS INNER JOIN NEWS_AUTHORS " +
            "ON NEWS.NEWS_NEWS_ID=NEWS_AUTHORS.NAUT_NEWS_ID WHERE NEWS_AUTHORS.NAUT_AUTHOR_ID=?" +
            "ORDER BY (SELECT COUNT(1) FROM COMMENTS WHERE COMMENTS.COMT_NEWS_ID=NEWS.NEWS_NEWS_ID) DESC";

    private static final String SQL_GET_NEWS_BY_TAGS_PREFIX = "SELECT NEWS.NEWS_NEWS_ID, NEWS.NEWS_TITLE, " +
            "NEWS.NEWS_SHORT_TEXT, NEWS.NEWS_FULL_TEXT, NEWS.NEWS_CREATION_DATE, NEWS.NEWS_MODIFICATION_DATE " +
            "FROM NEWS INNER JOIN NEWS_TAGS ON NEWS.NEWS_NEWS_ID=NEWS_TAGS.NTAG_NEWS_ID GROUP BY NEWS.NEWS_NEWS_ID, " +
            "NEWS.NEWS_TITLE, NEWS.NEWS_SHORT_TEXT, NEWS.NEWS_FULL_TEXT, NEWS.NEWS_CREATION_DATE, NEWS.NEWS_MODIFICATION_DATE " +
            "HAVING (SELECT COUNT(NEWS_TAGS.NTAG_TAG_ID) FROM NEWS_TAGS WHERE NEWS_TAGS.NTAG_TAG_ID IN (";
    private static final String SQL_GET_NEWS_BY_TAGS_POSTFIX = ") AND NEWS_TAGS.NTAG_NEWS_ID=NEWS.NEWS_NEWS_ID GROUP BY " +
            "NEWS_TAGS.NEWS_NEWS_ID)=? ORDER BY (SELECT COUNT(1) FROM COMMENTS WHERE " +
            "COMMENTS.COMT_NEWS_ID=NEWS.NEWS_NEWS_ID) DESC";

    private static final String SQL_GET_NEWS_BY_TAGS_AND_AUTHOR_PREFIX = "SELECT NEWS.NEWS_NEWS_ID, NEWS.NEWS_TITLE, " +
            "NEWS.NEWS_SHORT_TEXT, NEWS.NEWS_FULL_TEXT, NEWS.NEWS_CREATION_DATE, NEWS.NEWS_MODIFICATION_DATE FROM NEWS INNER " +
            "JOIN NEWS_TAGS ON NEWS.NEWS_NEWS_ID=NEWS_TAGS.NTAG_NEWS_ID INNER JOIN NEWS_AUTHORS " +
            "ON NEWS.NEWS_NEWS_ID=NEWS_AUTHORS.NAUT_NEWS_ID WHERE NEWS_AUTHORS.NAUT_AUTHOR_ID=? GROUP BY NEWS.NEWS_NEWS_ID, " +
            "NEWS.NEWS_TITLE, NEWS.NEWS_SHORT_TEXT, NEWS.NEWS_FULL_TEXT, NEWS.NEWS_CREATION_DATE, NEWS.NEWS_MODIFICATION_DATE " +
            "HAVING (SELECT COUNT(NEWS_TAGS.NTAG_TAG_ID) FROM NEWS_TAGS WHERE NEWS_TAGS.NTAG_TAG_ID IN (";
    private static final String SQL_GET_NEWS_BY_TAGS_AND_AUTHOR_POSTFIX = ") AND NEWS_TAGS.NTAG_NEWS_ID=NEWS.NEWS_NEWS_ID GROUP BY " +
            "NEWS_TAGS.NTAG_NEWS_ID)=? ORDER BY (SELECT COUNT(1) FROM COMMENTS WHERE COMMENTS.COMT_NEWS_ID=NEWS.NEWS_NEWS_ID) " +
            "DESC";

    private static final String SQL_EDIT_NEWS = "UPDATE NEWS SET NEWS.NEWS_TITLE=?, NEWS.NEWS_SHORT_TEXT=?, NEWS.NEWS_FULL_TEXT=?, " +
            "NEWS.NEWS_MODIFICATION_DATE=? WHERE NEWS.NEWS_NEWS_ID=?";
    private static final String SQL_DELETE_NEWS_BY_ID = "DELETE FROM NEWS WHERE NEWS.NEWS_NEWS_ID=?";

    private static final String SQL_DELETE_NEWS_BY_IDS_PREFIX = "DELETE FROM NEWS WHERE NEWS.NEWS_NEWS_ID IN(";
    private static final String SQL_DELETE_NEWS_BY_IDS_POSTFIX = ")";

    private static final String SQL_COUNT_ALL_NEWS = "SELECT COUNT(NEWS.NEWS_NEWS_ID) FROM NEWS";

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#add(Object)
     */
    @Override
    public long add(News news) throws DaoException {
        long newsId = 0;
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_ADD_NEWS, COLUMN_NAMES);
            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortText());
            ps.setString(3, news.getFullText());
            ps.setTimestamp(4, TimeDateGenerator.getCurrentTimestamp());
            ps.setDate(5, TimeDateGenerator.getCurrentDate());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            while (rs.next()) {
                newsId = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by adding news", e);
        } finally {
            closeResources(connection, ps);
        }
        return newsId;
    }

    /**
     * @see com.dev.newsmanagement.dao.NewsDao#getAllNews()
     */
    @Override
    public List<News> getAllNews() throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        List<News> newsList = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_GET_ALL_NEWS);
            ResultSet rs = ps.executeQuery();
            newsList = new ArrayList<>();
            while (rs.next()) {
                newsList.add(setupNewsFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting all news", e);
        } finally {
            closeResources(connection, ps);
        }
        return newsList;
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#get(long)
     */
    @Override
    public News get(long newsId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        News news = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_GET_NEWS_BY_NEWS_ID);
            ps.setLong(1, newsId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                news = setupNewsFields(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting news by news id", e);
        } finally {
            closeResources(connection, ps);
        }
        return news;
    }

    /**
     * @see com.dev.newsmanagement.dao.NewsDao#getNewsByTagsIds(java.util.List)
     */
    @Override
    public List<News> getNewsByTagsIds(List<Tag> tags) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        List<News> newsList = null;
        long[]tagIds=getTagIds(tags);
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(getResultQuery(SQL_GET_NEWS_BY_TAGS_PREFIX, SQL_GET_NEWS_BY_TAGS_POSTFIX,
                    tagIds.length));

            for (int i = 1; i <= tagIds.length; i++) {
                ps.setLong(i, tagIds[i - 1]);
            }
            ps.setInt(tagIds.length + 1, tagIds.length);

            ResultSet rs = ps.executeQuery();
            newsList = new ArrayList<>();
            while (rs.next()) {
                newsList.add(setupNewsFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting news by tag ids", e);
        } finally {
            closeResources(connection, ps);
        }
        return newsList;
    }

    /**
     * @see com.dev.newsmanagement.dao.NewsDao#getNewsByAuthorId(long)
     */
    @Override
    public List<News> getNewsByAuthorId(long authorId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        List<News> newsList = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_GET_NEWS_BY_AUTHOR_ID);
            ps.setLong(1, authorId);
            ResultSet rs = ps.executeQuery();
            newsList = new ArrayList<>();
            while (rs.next()) {
                newsList.add(setupNewsFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting news by author id", e);
        } finally {
            closeResources(connection, ps);
        }
        return newsList;
    }

    /**
     * @see com.dev.newsmanagement.dao.NewsDao#getNewsByTagIdsAndAuthorId(com.dev.newsmanagement.entity.SearchEntity)
     */
    @Override
    public List<News> getNewsByTagIdsAndAuthorId(SearchEntity searchEntity) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        List<News> newsList = null;
        long authorId = searchEntity.getAuthor().getAuthorId();
        long[] tagIds = getTagIds(searchEntity.getTags());
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(getResultQuery(SQL_GET_NEWS_BY_TAGS_AND_AUTHOR_PREFIX,
                    SQL_GET_NEWS_BY_TAGS_AND_AUTHOR_POSTFIX, tagIds.length));

            ps.setLong(1, authorId);
            for (int i = 2; i <= tagIds.length + 1; i++) {
                ps.setLong(i, tagIds[i - 2]);
            }
            ps.setInt(tagIds.length + 2, tagIds.length);

            ResultSet rs = ps.executeQuery();
            newsList = new ArrayList<>();
            while (rs.next()) {
                newsList.add(setupNewsFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting news by tag ids and author", e);
        } finally {
            closeResources(connection, ps);
        }
        return newsList;
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#edit(Object)
     */
    @Override
    public void edit(News modifiedNews) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_EDIT_NEWS);
            ps.setString(1, modifiedNews.getTitle());
            ps.setString(2, modifiedNews.getShortText());
            ps.setString(3, modifiedNews.getFullText());
            ps.setDate(4, TimeDateGenerator.getCurrentDate());
            ps.setLong(5, modifiedNews.getNewsId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by editing news", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#delete(long)
     */
    @Override
    public void delete(long newsId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_DELETE_NEWS_BY_ID);
            ps.setLong(1, newsId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by deleting news by news id", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.NewsDao#deleteNews(long...)
     */
    @Override
    public void deleteNews(long... newsIds) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(getResultQuery(SQL_DELETE_NEWS_BY_IDS_PREFIX, SQL_DELETE_NEWS_BY_IDS_POSTFIX, newsIds.length));
            for (int i = 1; i <= newsIds.length; i++) {
                ps.setLong(i, newsIds[i - 1]);
            }
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by deleting news by news ids", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.NewsDao#countAllNews()
     */
    @Override
    public long countAllNews() throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        long newsQuantity = 0;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_COUNT_ALL_NEWS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                newsQuantity = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by counting news quantity", e);
        } finally {
            closeResources(connection, ps);
        }
        return newsQuantity;
    }

    /**
     * Sets up fields of news instance
     *
     * @param rs
     * @return news instance with filled fields
     * @throws SQLException
     */
    private News setupNewsFields(ResultSet rs) throws SQLException {
        News news = new News();
        news.setNewsId(rs.getLong(NEWS_ID));
        news.setTitle(rs.getString(TITLE));
        news.setShortText(rs.getString(SHORT_TEXT));
        news.setFullText(rs.getString(FULL_TEXT));
        news.setCreationDate(rs.getTimestamp(CREATION_DATE));
        news.setModificationDate(rs.getDate(MODIFICATION_DATE));
        return news;
    }
}