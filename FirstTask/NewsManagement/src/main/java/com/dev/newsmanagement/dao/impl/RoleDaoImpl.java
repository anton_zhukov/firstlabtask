package com.dev.newsmanagement.dao.impl;

import com.dev.newsmanagement.dao.RoleDao;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Role;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleDaoImpl extends CommonDao implements RoleDao {

    private static final String[] COLUMN_NAMES = new String[]{"ROL_ROLE_ID"};

    private static final String SQL_ADD_ROLE = "INSERT INTO ROLES (ROL_ROLE_ID, ROL_USER_ID, ROL_ROLE_NAME) VALUES " +
            "(ROL_SEQ.NEXTVAL, ?, ?)";

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#add(Object)
     */
    @Override
    public long add(Role role) throws DaoException {
        long roleId = 0;
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_ADD_ROLE, COLUMN_NAMES);
            ps.setLong(1, role.getUserId());
            ps.setString(2, role.getRoleName());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            while (rs.next()) {
                roleId = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by adding role", e);
        } finally {
            closeResources(connection, ps);
        }
        return roleId;
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#get(long)
     */
    @Override
    public Role get(long id) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#edit(Object)
     */
    @Override
    public void edit(Role role) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#delete(long)
     */
    @Override
    public void delete(long id) throws DaoException {
        throw new UnsupportedOperationException();
    }
}