package com.dev.newsmanagement.dao.impl;

import com.dev.newsmanagement.dao.TagDao;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Tag;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TagDaoImpl extends CommonDao implements TagDao {

    private static final String TAG_ID = "TAG_TAG_ID";
    private static final String TAG_NAME = "TAG_TAG_NAME";
    private static final String[] COLUMN_NAMES = new String[]{TAG_ID};

    private final String SQL_ADD_TAG = "INSERT INTO TAGS (TAG_TAG_ID, TAG_TAG_NAME) VALUES(TAG_SEQ.NEXTVAL, ?)";
    private final String SQL_ADD_TAG_TO_NEWS = "INSERT INTO NEWS_TAGS (NTAG_NEWS_ID, NTAG_TAG_ID) " +
            "VALUES(?, ?)";
    private final String SQL_GET_TAG_BY_TAG_ID = "SELECT TAGS.TAG_TAG_ID, TAGS.TAG_TAG_NAME FROM TAGS WHERE TAGS.TAG_TAG_ID=?";
    private final String SQL_GET_TAGS_BY_NEWS_ID = "SELECT TAGS.TAG_TAG_ID, TAGS.TAG_TAG_NAME FROM TAGS INNER JOIN NEWS_TAGS ON " +
            "TAGS.TAG_TAG_ID=NEWS_TAGS.NTAG_TAG_ID WHERE NEWS_TAGS.NTAG_NEWS_ID=?";
    private final String SQL_GET_ALL_TAGS = "SELECT TAGS.TAG_TAG_ID, TAGS.TAG_TAG_NAME FROM TAGS";
    private static final String SQL_EDIT_TAG_NAME = "UPDATE TAGS SET TAGS.TAG_TAG_NAME=? WHERE TAGS.TAG_TAG_ID=?";
    private static final String SQL_DELETE_TAG = "DELETE FROM TAGS WHERE TAGS.TAG_TAG_ID=?";
    private static final String SQL_DELETE_TAG_FROM_ALL_NEWS = "DELETE FROM NEWS_TAGS WHERE NEWS_TAGS.NTAG_TAG_ID=?";
    private static final String SQL_DELETE_TAGS_FROM_NEWS_BY_IDS_PREFIX = "DELETE FROM NEWS_TAGS WHERE NEWS_TAGS.NTAG_NEWS_ID IN(";
    private static final String SQL_DELETE_TAGS_FROM_NEWS_BY_IDS_POSTFIX = ")";

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#add(Object)
     */
    @Override
    public long add(Tag tag) throws DaoException {
        long tagId = 0;
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_ADD_TAG, COLUMN_NAMES);
            ps.setString(1, tag.getTagName());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            while (rs.next()) {
                tagId = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by adding tag", e);
        } finally {
            closeResources(connection, ps);
        }
        return tagId;
    }

    /**
     * @see com.dev.newsmanagement.dao.TagDao#addTagsToNews(long, java.util.List)
     */
    @Override
    public void addTagsToNews(long newsId, List<Tag> tags) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        long[] tagsIds = getTagIds(tags);
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_ADD_TAG_TO_NEWS);
            for (long tagId : tagsIds) {
                ps.setLong(1, newsId);
                ps.setLong(2, tagId);
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException("Error by adding tags to news", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#get(long)
     */
    @Override
    public Tag get(long tagId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        Tag tag = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_GET_TAG_BY_TAG_ID);
            ps.setLong(1, tagId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                tag = setupTagFields(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting tag by tag id", e);
        } finally {
            closeResources(connection, ps);
        }
        return tag;
    }

    /**
     * @see com.dev.newsmanagement.dao.TagDao#getNewsTagsByNewsId(long)
     */
    @Override
    public List<Tag> getNewsTagsByNewsId(long newsId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        List<Tag> tags = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_GET_TAGS_BY_NEWS_ID);
            ps.setLong(1, newsId);
            ResultSet rs = ps.executeQuery();
            tags = new ArrayList<>();
            while (rs.next()) {
                tags.add(setupTagFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting news tags by news id");
        } finally {
            closeResources(connection, ps);
        }
        return tags;
    }

    /**
     * @see com.dev.newsmanagement.dao.TagDao#getAllTags()
     */
    @Override
    public List<Tag> getAllTags() throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        List<Tag> tags = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_GET_ALL_TAGS);
            ResultSet rs = ps.executeQuery();
            tags = new ArrayList<>();
            while (rs.next()) {
                tags.add(setupTagFields(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Error by getting all tags", e);
        } finally {
            closeResources(connection, ps);
        }
        return tags;
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#edit(Object)
     */
    @Override
    public void edit(Tag modifiedTag) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_EDIT_TAG_NAME);
            ps.setString(1, modifiedTag.getTagName());
            ps.setLong(2, modifiedTag.getTagId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by editing tag", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#delete(long)
     */
    @Override
    public void delete(long tagId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_DELETE_TAG);
            ps.setLong(1, tagId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by deleting tag", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.TagDao#deleteTagFromAllNews(long)
     */
    @Override
    public void deleteTagFromAllNews(long tagId) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_DELETE_TAG_FROM_ALL_NEWS);
            ps.setLong(1, tagId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by deleting tag from news", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * @see com.dev.newsmanagement.dao.TagDao#deleteTagsFromNewsByIds(long...)
     */
    public void deleteTagsFromNewsByIds(long... newsIds) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(getResultQuery(SQL_DELETE_TAGS_FROM_NEWS_BY_IDS_PREFIX, SQL_DELETE_TAGS_FROM_NEWS_BY_IDS_POSTFIX,
                    newsIds.length));
            for (int i = 1; i <= newsIds.length; i++) {
                ps.setLong(i, newsIds[i - 1]);
            }
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Error by deleting tags from news", e);
        } finally {
            closeResources(connection, ps);
        }
    }

    /**
     * Sets up fields of tag instance
     *
     * @param rs
     * @return tag instance with filled fields
     * @throws SQLException
     */
    private Tag setupTagFields(ResultSet rs) throws SQLException {
        return new Tag(rs.getLong(TAG_ID), rs.getString(TAG_NAME));
    }
}