package com.dev.newsmanagement.dao.impl;

import com.dev.newsmanagement.dao.UsersDao;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.User;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsersDaoImpl extends CommonDao implements UsersDao {

    private static final String[] COLUMN_NAMES = new String[]{"USR_USER_ID"};

    private static final String SQL_ADD_USER = "INSERT INTO USERS (USR_USER_ID, USR_USER_NAME, USR_LOGIN, USR_PASSWORD) " +
            "VALUES (USR_SEQ.NEXTVAL, ?, ?, ?)";

    private static final String SQL_DOES_LOGIN_EXIST = "SELECT COUNT(USERS.USR_USER_ID) FROM USERS WHERE USERS.USR_LOGIN=?";
    private static final String SQL_DOES_USER_EXIST = "SELECT COUNT(USERS.USR_USER_ID) FROM USERS WHERE USERS.USR_LOGIN=? " +
            "AND USERS.PASSWORD=?";

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#add(Object)
     */
    @Override
    public long add(User user) throws DaoException {
        long userId = 0;
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_ADD_USER, COLUMN_NAMES);
            ps.setString(1, user.getUserName());
            ps.setString(2, user.getLogin());
            ps.setString(3, user.getPassword());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            while (rs.next()) {
                userId = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException("Error by adding user", e);
        } finally {
            closeResources(connection, ps);
        }
        return userId;
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#get(long)
     */
    @Override
    public User get(long id) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#edit(Object)
     */
    @Override
    public void edit(User user) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * @see com.dev.newsmanagement.dao.BasicDao#delete(long) (Object)
     */
    @Override
    public void delete(long id) throws DaoException {
        throw new UnsupportedOperationException();
    }

    /**
     * @see com.dev.newsmanagement.dao.UsersDao#doesLoginExist(String)
     */
    @Override
    public boolean doesLoginExist(String login) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        boolean doesExist = false;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_DOES_LOGIN_EXIST);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getLong(1) == 1) {
                    doesExist = true;
                }
            }
        } catch (SQLException e) {
            throw new DaoException("Error by checking whether login exists", e);
        } finally {
            closeResources(connection, ps);
        }
        return doesExist;
    }

    /**
     * @see com.dev.newsmanagement.dao.UsersDao#doesUserExist(String, String)
     */
    @Override
    public boolean doesUserExist(String login, String password) throws DaoException {
        Connection connection = null;
        PreparedStatement ps = null;
        boolean doesExist = false;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(SQL_DOES_USER_EXIST);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getLong(1) == 1) {
                    doesExist = true;
                }
            }
        } catch (SQLException e) {
            throw new DaoException("Error by checking whether user exists", e);
        } finally {
            closeResources(connection, ps);
        }
        return doesExist;
    }
}