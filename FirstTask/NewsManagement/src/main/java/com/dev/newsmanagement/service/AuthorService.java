package com.dev.newsmanagement.service;

import com.dev.newsmanagement.entity.Author;
import com.dev.newsmanagement.exception.ServiceException;

import java.util.List;

public interface AuthorService {

    /**
     * Adds author
     *
     * @param author
     * @return generated primary key
     * @throws ServiceException
     */
    long addAuthor(Author author) throws ServiceException;

    /**
     * Gets list of authors
     *
     * @return list of all authors
     * @throws ServiceException
     */
    List<Author> getAllAuthors() throws ServiceException;

    /**
     * Edits author
     *
     * @param modifiedAuthor
     * @throws ServiceException
     */
    void editAuthorName(Author modifiedAuthor) throws ServiceException;

    /**
     * Makes author expired
     *
     * @param author
     * @throws ServiceException
     */
    void makeAuthorExpired(Author author) throws ServiceException;
}