package com.dev.newsmanagement.service;

import com.dev.newsmanagement.entity.Comment;
import com.dev.newsmanagement.exception.ServiceException;

import java.util.List;

public interface CommentsService {

    /**
     * Adds comment
     *
     * @param comment
     * @return generated primary key
     * @throws ServiceException
     */
    long addComment(Comment comment) throws ServiceException;

    /**
     * Gets list of comments by news id
     *
     * @param newsId
     * @return list of comments, got by news id
     * @throws ServiceException
     */
    List<Comment> getCommentsByNewsId(long newsId) throws ServiceException;

    /**
     * Edits comments
     *
     * @param modifiedComment
     * @throws ServiceException
     */
    void editCommentText(Comment modifiedComment) throws ServiceException;

    /**
     * Deletes comment
     *
     * @param commentId
     * @throws ServiceException
     */
    void deleteComment(long commentId) throws ServiceException;

    /**
     * Counts number of comments in news
     *
     * @param newsId
     * @return number of comments in single news
     * @throws ServiceException
     */
    long countAllCommentsByNewsId(long newsId) throws ServiceException;
}