package com.dev.newsmanagement.service;

import com.dev.newsmanagement.entity.NewsTO;
import com.dev.newsmanagement.entity.SearchEntity;
import com.dev.newsmanagement.exception.ServiceException;

import java.util.List;

public interface NewsService {

    /**
     * Adds news
     *
     * @param newsTO
     * @throws ServiceException
     */
    long addNews(NewsTO newsTO) throws ServiceException;

    /**
     * Gets news details by id
     *
     * @param newsId
     * @return newsTO got by id
     * @throws ServiceException
     */
    NewsTO getNewsTO(long newsId) throws ServiceException;

    /**
     * Gets newsTO list
     *
     * @return list of all newsTO
     * @throws ServiceException
     */
    List<NewsTO> getNewsTOList() throws ServiceException;

    /**
     * Gets newsTO list found by author and tags
     *
     * @param searchEntity
     * @return list of news details found by tags and author
     * @throws ServiceException
     */
    List<NewsTO> getNewsTOListByAuthorAndTags(SearchEntity searchEntity) throws ServiceException;


    /**
     * Edits news
     *
     * @param modifiedNews
     * @throws ServiceException
     */
    void editNews(NewsTO modifiedNews) throws ServiceException;

    /**
     * Deletes news by ids
     *
     * @param newsIds
     * @throws ServiceException
     */
    void deleteNews(long... newsIds) throws ServiceException;

    /**
     * Counts number of news
     *
     * @return number of news
     * @throws ServiceException
     */
    long countNews() throws ServiceException;
}