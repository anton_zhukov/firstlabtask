package com.dev.newsmanagement.service;

import com.dev.newsmanagement.entity.Tag;
import com.dev.newsmanagement.exception.ServiceException;

import java.util.List;

public interface TagService {

    /**
     * Adds tag
     *
     * @param tag
     * @return generated primary key
     * @throws ServiceException
     */
    long addTag(Tag tag) throws ServiceException;

    /**
     * Gets list of all tags
     *
     * @return list of all tags
     * @throws ServiceException
     */
    List<Tag> getAllTags() throws ServiceException;

    /**
     * Edits tag
     *
     * @param modifiedTag
     * @throws ServiceException
     */
    void editTag(Tag modifiedTag) throws ServiceException;

    /**
     * Deletes tag
     *
     * @param tagId
     * @throws ServiceException
     */
    void deleteTag(long tagId) throws ServiceException;
}