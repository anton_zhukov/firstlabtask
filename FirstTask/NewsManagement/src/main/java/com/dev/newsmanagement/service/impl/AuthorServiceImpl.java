package com.dev.newsmanagement.service.impl;

import com.dev.newsmanagement.dao.AuthorDao;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Author;
import com.dev.newsmanagement.service.AuthorService;
import com.dev.newsmanagement.exception.ServiceException;

import java.util.List;

public class AuthorServiceImpl implements AuthorService {

    private AuthorDao authorDao;

    /**
     * @see com.dev.newsmanagement.service.AuthorService#addAuthor(com.dev.newsmanagement.entity.Author)
     */
    @Override
    public long addAuthor(Author author) throws ServiceException {
        long authorId = 0;
        try {
            authorId = authorDao.add(author);
        } catch (DaoException e) {
            throw new ServiceException("Error by adding author", e);
        }
        return authorId;
    }

    /**
     * @see com.dev.newsmanagement.service.AuthorService#getAllAuthors()
     */
    @Override
    public List<Author> getAllAuthors() throws ServiceException {
        List<Author> authors = null;
        try {
            authors = authorDao.getAllAuthors();
        } catch (DaoException e) {
            throw new ServiceException("Error by getting all authors", e);
        }
        return authors;
    }

    /**
     * @see com.dev.newsmanagement.service.AuthorService#editAuthorName(com.dev.newsmanagement.entity.Author)
     */
    @Override
    public void editAuthorName(Author modifiedAuthor) throws ServiceException {
        try {
            authorDao.edit(modifiedAuthor);
        } catch (DaoException e) {
            throw new ServiceException("Error by editing author", e);
        }
    }

    /**
     * @see com.dev.newsmanagement.service.AuthorService#makeAuthorExpired(com.dev.newsmanagement.entity.Author)
     */
    @Override
    public void makeAuthorExpired(Author author) throws ServiceException {
        try {
            authorDao.makeAuthorExpired(author.getAuthorId());
        } catch (DaoException e) {
            throw new ServiceException("Error by making author expired", e);
        }
    }

    public void setAuthorDao(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }
}