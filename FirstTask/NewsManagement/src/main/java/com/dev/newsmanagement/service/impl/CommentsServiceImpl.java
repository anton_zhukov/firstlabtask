package com.dev.newsmanagement.service.impl;

import com.dev.newsmanagement.dao.CommentsDao;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Comment;
import com.dev.newsmanagement.service.CommentsService;
import com.dev.newsmanagement.exception.ServiceException;

import java.util.List;

public class CommentsServiceImpl implements CommentsService {

    private CommentsDao commentsDao;

    /**
     * @see com.dev.newsmanagement.service.CommentsService#addComment(com.dev.newsmanagement.entity.Comment)
     */
    @Override
    public long addComment(Comment comment) throws ServiceException {
        long commentId = 0;
        try {
            commentId = commentsDao.add(comment);
        } catch (DaoException e) {
            throw new ServiceException("Error by adding news", e);
        }
        return commentId;
    }

    /**
     * @see com.dev.newsmanagement.service.CommentsService#getCommentsByNewsId(long)
     */
    @Override
    public List<Comment> getCommentsByNewsId(long newsId) throws ServiceException {
        List<Comment> comments = null;
        try {
            comments = commentsDao.getCommentsByNewsId(newsId);
        } catch (DaoException e) {
            throw new ServiceException("Error by getting comments by news id", e);
        }
        return comments;
    }

    /**
     * @see com.dev.newsmanagement.service.CommentsService#editCommentText(com.dev.newsmanagement.entity.Comment)
     */
    @Override
    public void editCommentText(Comment modifiedComment) throws ServiceException {
        try {
            commentsDao.edit(modifiedComment);
        } catch (DaoException e) {
            throw new ServiceException("Error by editing comment", e);
        }
    }

    /**
     * @see com.dev.newsmanagement.service.CommentsService#deleteComment(long)
     */
    @Override
    public void deleteComment(long commentId) throws ServiceException {
        try {
            commentsDao.delete(commentId);
        } catch (DaoException e) {
            throw new ServiceException("Error by deleting comment", e);
        }
    }

    /**
     * @see com.dev.newsmanagement.service.CommentsService#countAllCommentsByNewsId(long)
     */
    @Override
    public long countAllCommentsByNewsId(long newsId) throws ServiceException {
        long commentsNumber = 0;
        try {
            commentsNumber = commentsDao.countAllCommentsByNewsId(newsId);
        } catch (DaoException e) {
            throw new ServiceException("Error by counting comments quantity by news id", e);
        }
        return commentsNumber;
    }

    public void setCommentsDao(CommentsDao commentsDao) {
        this.commentsDao = commentsDao;
    }
}