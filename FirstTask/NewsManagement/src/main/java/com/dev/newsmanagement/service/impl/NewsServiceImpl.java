package com.dev.newsmanagement.service.impl;

import com.dev.newsmanagement.dao.AuthorDao;
import com.dev.newsmanagement.dao.CommentsDao;
import com.dev.newsmanagement.dao.NewsDao;
import com.dev.newsmanagement.dao.TagDao;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.*;
import com.dev.newsmanagement.service.NewsService;
import com.dev.newsmanagement.exception.ServiceException;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class NewsServiceImpl implements NewsService {

    private NewsDao newsDao;
    private AuthorDao authorDao;
    private TagDao tagDao;
    private CommentsDao commentsDao;

    /**
     * @see com.dev.newsmanagement.service.NewsService#addNews(com.dev.newsmanagement.entity.NewsTO)
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public long addNews(NewsTO newsTO) throws ServiceException {
        long newsId;
        try {
            newsId = newsDao.add(newsTO.getNews());
            authorDao.addAuthorToNews(newsId, newsTO.getAuthor());
            tagDao.addTagsToNews(newsId, newsTO.getTags());
        } catch (DaoException e) {
            throw new ServiceException("Error by adding news", e);
        }
        return newsId;
    }

    /**
     * @see com.dev.newsmanagement.service.NewsService#getNewsTO(long)
     */
    @Override
    public NewsTO getNewsTO(long newsId) throws ServiceException {
        NewsTO newsTO;
        try {
            newsTO = setupNewsTOFields(newsId);
        } catch (DaoException e) {
            throw new ServiceException("Error by getting news details", e);
        }
        return newsTO;
    }

    /**
     * @see com.dev.newsmanagement.service.NewsService#getNewsTOList()
     */
    @Override
    public List<NewsTO> getNewsTOList() throws ServiceException {
        List<NewsTO> newsTOList;
        try {
            newsTOList = new ArrayList<>();
            List<News> newsList = newsDao.getAllNews();
            for (News news : newsList) {
                newsTOList.add(setupNewsTOList(news));
            }
        } catch (DaoException e) {
            throw new ServiceException("Error by getting news details list", e);
        }
        return newsTOList;
    }

    /**
     * @see com.dev.newsmanagement.service.NewsService#getNewsTOListByAuthorAndTags(com.dev.newsmanagement.entity.SearchEntity)
     */
    @Override
    public List<NewsTO> getNewsTOListByAuthorAndTags(SearchEntity searchEntity) throws ServiceException {
        List<NewsTO> newsTOList;
        List<News> newsList;
        try {
            newsTOList = new ArrayList<>();
            if (searchEntity.getAuthor().getAuthorId() == 0 & searchEntity.getTags().isEmpty()) {
                newsList = newsDao.getAllNews();
            } else if (searchEntity.getTags().isEmpty()) {
                newsList = newsDao.getNewsByAuthorId(searchEntity.getAuthor().getAuthorId());
            } else if (searchEntity.getAuthor().getAuthorId() == 0) {
                newsList = newsDao.getNewsByTagsIds(searchEntity.getTags());
            } else {
                newsList = newsDao.getNewsByTagIdsAndAuthorId(searchEntity);
            }

            for (News news : newsList) {
                newsTOList.add(setupNewsTOList(news));
            }
        } catch (DaoException e) {
            throw new ServiceException("Error by getting news details list by author and tags", e);
        }
        return newsTOList;
    }


    /**
     * @see com.dev.newsmanagement.service.NewsService#editNews(com.dev.newsmanagement.entity.NewsTO)
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editNews(NewsTO modifiedNews) throws ServiceException {
        try {
            newsDao.edit(modifiedNews.getNews());
            authorDao.editNewsAuthor(modifiedNews.getAuthor(), modifiedNews.getNews());
            tagDao.deleteTagsFromNewsByIds(modifiedNews.getNews().getNewsId());
            tagDao.addTagsToNews(modifiedNews.getNews().getNewsId(), modifiedNews.getTags());
        } catch (DaoException e) {
            throw new ServiceException("Error by editing news", e);
        }
    }

    /**
     * @see com.dev.newsmanagement.service.NewsService#deleteNews(long...)
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteNews(long... newsIds) throws ServiceException {
        try {
            commentsDao.deleteCommentsFromNews(newsIds);
            authorDao.deleteAuthorFromNews(newsIds);
            tagDao.deleteTagsFromNewsByIds(newsIds);
            newsDao.deleteNews(newsIds);
        } catch (DaoException e) {
            throw new ServiceException("Error by deleting news", e);
        }
    }

    /**
     * @see com.dev.newsmanagement.service.NewsService#countNews()
     */
    @Override
    public long countNews() throws ServiceException {
        long newsNumber;
        try {
            newsNumber = newsDao.countAllNews();
        } catch (DaoException e) {
            throw new ServiceException("Error by counting news quantity", e);
        }
        return newsNumber;
    }

    /**
     * Sets up fields of newsTO instance
     *
     * @param newsId
     * @return newsTO instance with filled fields
     * @throws DaoException
     */
    private NewsTO setupNewsTOFields(long newsId) throws DaoException {
        NewsTO newsTO = new NewsTO();

        News news = newsDao.get(newsId);
        newsTO.setNews(news);
        setupCommonFields(newsId, newsTO);

        return newsTO;
    }

    /**
     * Sets up fields of newsTO instance
     *
     * @param news
     * @return newsTO instance with filled fields
     * @throws DaoException
     */
    private NewsTO setupNewsTOList(News news) throws DaoException {
        NewsTO newsTO = new NewsTO();

        newsTO.setNews(news);
        setupCommonFields(news.getNewsId(), newsTO);

        return newsTO;
    }

    /**
     * Help method for method "setupNewsTOList"
     *
     * @param newsId
     * @param newsTO
     * @throws DaoException
     */
    private void setupCommonFields(long newsId, NewsTO newsTO) throws DaoException {
        Author author = authorDao.getNewsAuthorByNewsId(newsId);
        List<Tag> tags = tagDao.getNewsTagsByNewsId(newsId);
        List<Comment> comments = commentsDao.getCommentsByNewsId(newsId);

        newsTO.setAuthor(author);
        newsTO.setTags(tags);
        newsTO.setComments(comments);
    }

    public void setNewsDao(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    public void setAuthorDao(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

    public void setTagDao(TagDao tagDao) {
        this.tagDao = tagDao;
    }

    public void setCommentsDao(CommentsDao commentsDao) {
        this.commentsDao = commentsDao;
    }

}