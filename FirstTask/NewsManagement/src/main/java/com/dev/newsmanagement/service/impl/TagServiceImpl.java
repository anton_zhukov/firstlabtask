package com.dev.newsmanagement.service.impl;

import com.dev.newsmanagement.dao.TagDao;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Tag;
import com.dev.newsmanagement.service.TagService;
import com.dev.newsmanagement.exception.ServiceException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class TagServiceImpl implements TagService {

    private TagDao tagDao;

    /**
     * @see com.dev.newsmanagement.service.TagService#addTag(com.dev.newsmanagement.entity.Tag)
     */
    @Override
    public long addTag(Tag tag) throws ServiceException {
        long tagId = 0;
        try {
            tagId = tagDao.add(tag);
        } catch (DaoException e) {
            throw new ServiceException("Error by adding tag", e);
        }
        return tagId;
    }

    /**
     * @see com.dev.newsmanagement.service.TagService#getAllTags()
     */
    @Override
    public List<Tag> getAllTags() throws ServiceException {
        List<Tag> tags = null;
        try {
            tags = tagDao.getAllTags();
        } catch (DaoException e) {
            throw new ServiceException("Error by getting all tags", e);
        }
        return tags;
    }

    /**
     * @see com.dev.newsmanagement.service.TagService#editTag(com.dev.newsmanagement.entity.Tag)
     */
    @Override
    public void editTag(Tag modifiedTag) throws ServiceException {
        try {
            tagDao.edit(modifiedTag);
        } catch (DaoException e) {
            throw new ServiceException("Error by editing tag", e);
        }
    }

    /**
     * @see com.dev.newsmanagement.service.TagService#deleteTag(long)
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteTag(long tagId) throws ServiceException {
        try {
            tagDao.deleteTagFromAllNews(tagId);
            tagDao.delete(tagId);
        } catch (DaoException e) {
            throw new ServiceException("Error by deleting tag", e);
        }
    }

   public void setTagDao(TagDao tagDao) {
        this.tagDao = tagDao;
    }
}