package com.dev.newsmanagement.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

public class TimeDateGenerator {

    /**
     * Generates modification date for news
     *
     * @return new java.sql.Date
     */
    public static Date getCurrentDate() {
        return new Date(Calendar.getInstance().getTimeInMillis());
    }

    /**
     * Generates creation date for news and comment, expiration date for authors
     *
     * @return new java.sql.Timestamp
     */
    public static Timestamp getCurrentTimestamp() {
        return new Timestamp(Calendar.getInstance().getTimeInMillis());
    }
}