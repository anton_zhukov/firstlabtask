package com.dev.newsmanagement.dao;

import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.Arrays;
import java.util.List;
import org.junit.Ignore;
import static org.junit.Assert.*;
import static org.unitils.reflectionassert.ReflectionAssert.assertPropertyLenientEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/testApplicationContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@Ignore
public class AuthorDaoTest {

    @Autowired
    private AuthorDao authorDao;

    @Test
    @DatabaseSetup(value = "/testData/AuthorDaoTest.xml")
    @DatabaseTearDown(value = "/testData/AuthorDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testAdd() {
        try {
            Author expectedAuthor = new Author("TestUserThree");
            long authorId = authorDao.add(expectedAuthor);
            Author actualAuthor = authorDao.get(authorId);
            assertEquals(expectedAuthor.getAuthorName(), actualAuthor.getAuthorName());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/AuthorDaoTest.xml")
    @DatabaseTearDown(value = "/testData/AuthorDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testGetAllAuthors() {
        List<Author> authors = null;
        try {
            authors = authorDao.getAllAuthors();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        assertPropertyLenientEquals("authorId", Arrays.asList(1, 2), authors);
    }

    @Test
    @DatabaseSetup(value = "/testData/AuthorDaoTest.xml")
    @DatabaseTearDown(value = "/testData/AuthorDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testEdit() {
        try {
            List<Author> result = authorDao.getAllAuthors();
            Author expectedAuthor = result.stream().filter(a -> a.getAuthorId() == 1).findFirst().get();

            long authorId = expectedAuthor.getAuthorId();
            String editedAuthorName = "EditedTestUserOne";

            expectedAuthor.setAuthorName(editedAuthorName);

            authorDao.edit(expectedAuthor);
            Author actualAuthor = authorDao.get(authorId);
            assertEquals(expectedAuthor.getAuthorId(), actualAuthor.getAuthorId());
            assertEquals(expectedAuthor.getAuthorName(), actualAuthor.getAuthorName());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/AuthorDaoTest.xml")
    @DatabaseTearDown(value = "/testData/AuthorDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testMakeAuthorExpired() {
        try {
            List<Author> result = authorDao.getAllAuthors();
            Author expectedAuthor = result.stream().filter(a -> a.getExpired() == null).findFirst().get();

            long authorId = expectedAuthor.getAuthorId();
            expectedAuthor.setAuthorId(authorId);
            authorDao.makeAuthorExpired(expectedAuthor.getAuthorId());

            Author actualAuthor = authorDao.get(authorId);
            assertNull(expectedAuthor.getExpired());
            assertNotNull(actualAuthor.getExpired());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/AuthorDaoTest.xml")
    @DatabaseTearDown(value = "/testData/AuthorDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testAddAuthorForNews() {
        long newsId = 2L;
        long authorId = 2L;
        Author expectedAuthor = new Author("TestUserTwo");
        expectedAuthor.setAuthorId(authorId);
        try {
            authorDao.addAuthorToNews(newsId, expectedAuthor);
            Author actualAuthor = authorDao.getNewsAuthorByNewsId(newsId);
            assertReflectionEquals("TestUserTwo", actualAuthor.getAuthorName());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }
}