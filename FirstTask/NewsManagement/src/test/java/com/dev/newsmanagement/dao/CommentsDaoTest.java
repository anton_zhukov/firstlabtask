package com.dev.newsmanagement.dao;

import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Comment;
import com.dev.newsmanagement.util.TimeDateGenerator;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import org.junit.Ignore;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.unitils.reflectionassert.ReflectionAssert.assertPropertyLenientEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/testApplicationContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@Ignore
public class CommentsDaoTest {

    @Autowired
    private CommentsDao commentsDao;

    @Test
    @DatabaseSetup(value = "/testData/CommentsDaoTest.xml")
    @DatabaseTearDown(value = "/testData/CommentsDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testAddComment() {
        long newsId = 1;
        Comment expectedComment = new Comment();
        expectedComment.setNewsId(newsId);
        expectedComment.setCommentText("TestCommentText");
        Timestamp creationDate = TimeDateGenerator.getCurrentTimestamp();
        expectedComment.setCreationDate(creationDate);
        try {
            long commentId = commentsDao.add(expectedComment);
            Comment actualComment = commentsDao.get(commentId);
            assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
            assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/CommentsDaoTest.xml")
    @DatabaseTearDown(value = "/testData/CommentsDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testGetCommentsByNewsId() {
        long newsId = 1L;
        List<Comment> comments = null;
        try {
            comments = commentsDao.getCommentsByNewsId(newsId);
        } catch (DaoException e) {
            e.printStackTrace();
        }
        assertPropertyLenientEquals("commentId", Arrays.asList(1, 2), comments);
    }

    @Test
    @DatabaseSetup(value = "/testData/CommentsDaoTest.xml")
    @DatabaseTearDown(value = "/testData/CommentsDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testEditCommentText() {
        long commentId = 1L;
        long newsId = 1L;
        Comment expectedComment = new Comment();
        expectedComment.setCommentId(commentId);
        expectedComment.setNewsId(newsId);
        expectedComment.setCommentText("Edited");
        String timestamp = "2016-02-03 12:14:19";
        Timestamp creationDate = Timestamp.valueOf(timestamp);
        expectedComment.setCreationDate(creationDate);
        try {
            commentsDao.edit(expectedComment);
            Comment actualComment = commentsDao.get(commentId);
            assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
            assertEquals(expectedComment.getCreationDate(), actualComment.getCreationDate());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/CommentsDaoTest.xml")
    @DatabaseTearDown(value = "/testData/CommentsDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testDeleteComment() {
        long commentId = 1L;
        try {
            commentsDao.delete(commentId);
            assertNull(commentsDao.get(commentId));
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/CommentsDaoTest.xml")
    @DatabaseTearDown(value = "/testData/CommentsDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testCountAllCommentsByNewsId() {
        long expected = 2L;
        long actual = 0;
        long newsId = 1;
        try {
            actual = commentsDao.countAllCommentsByNewsId(newsId);
        } catch (DaoException e) {
            e.printStackTrace();
        }
        assertEquals(expected, actual);
    }
}