package com.dev.newsmanagement.dao;

import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.sql.Date;
import java.sql.Timestamp;
import org.junit.Ignore;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/testApplicationContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@Ignore
public class NewsDaoTest {

    @Autowired
    private NewsDao newsDao;

    private static News news;

    @BeforeClass
    public static void init() {
        news = new News();
        String title = "Test title";
        String shortText = "Test short text";
        String fullText = "Test full text";

        news.setTitle(title);
        news.setShortText(shortText);
        news.setFullText(fullText);
    }

    @AfterClass
    public static void clear() {
        news = null;
    }

    @Test
    @DatabaseSetup(value = "/testData/NewsDaoTest.xml")
    @DatabaseTearDown(value = "/testData/NewsDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testAddNews() {
        News expectedNews = news;
        try {
            long newsId = newsDao.add(expectedNews);
            News actualNews = newsDao.get(newsId);
            assertEquals(expectedNews.getTitle(), actualNews.getTitle());
            assertEquals(expectedNews.getFullText(), actualNews.getFullText());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/NewsDaoTest.xml")
    @DatabaseTearDown(value = "/testData/NewsDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testGetSingleNewsByNewsId() {
        long newsId = 1L;
        News expectedNews = news;
        news.setNewsId(newsId);
        String timestamp = "2016-02-03 12:14:19";
        String date = "2016-02-04";
        Timestamp creationDate = Timestamp.valueOf(timestamp);
        Date modificationDate = Date.valueOf(date);
        expectedNews.setCreationDate(creationDate);
        expectedNews.setModificationDate(modificationDate);
        try {
            News actualNews = newsDao.get(newsId);
            assertEquals(expectedNews.getTitle(), actualNews.getTitle());
            assertEquals(expectedNews.getFullText(), actualNews.getFullText());
            assertEquals(expectedNews.getCreationDate(), actualNews.getCreationDate());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/NewsDaoTest.xml")
    @DatabaseTearDown(value = "/testData/NewsDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testEditNews() {
        long newsId = 1L;
        News expectedNews = new News();
        expectedNews.setNewsId(newsId);
        expectedNews.setTitle("Edited");
        expectedNews.setShortText("Edited");
        expectedNews.setFullText("Edited");
        String timestamp = "2016-02-03 12:14:19";
        String date = "2016-02-04";
        Timestamp creationDate = Timestamp.valueOf(timestamp);
        Date modificationDate = Date.valueOf(date);
        expectedNews.setCreationDate(creationDate);
        expectedNews.setModificationDate(modificationDate);
        try {
            newsDao.edit(expectedNews);
            News actualNews = newsDao.get(newsId);
            assertEquals(expectedNews.getTitle(), actualNews.getTitle());
            assertEquals(expectedNews.getShortText(), actualNews.getShortText());
            assertEquals(expectedNews.getFullText(), actualNews.getFullText());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/NewsDaoTest.xml")
    @DatabaseTearDown(value = "/testData/NewsDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testDeleteNews() {
        long newsIdFirst = 1L;
        long newsIdSecond = 2L;
        try {
            newsDao.deleteNews(newsIdFirst, newsIdSecond);
            assertNull(newsDao.get(newsIdFirst));
            assertNull(newsDao.get(newsIdSecond));
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/NewsDaoTest.xml")
    @DatabaseTearDown(value = "/testData/NewsDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testCountAllNews() {
        long i = 0;
        try {
            i = newsDao.countAllNews();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        assertEquals(i, 2);
    }
}