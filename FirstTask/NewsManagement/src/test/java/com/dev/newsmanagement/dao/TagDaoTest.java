package com.dev.newsmanagement.dao;

import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Ignore;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.unitils.reflectionassert.ReflectionAssert.assertPropertyLenientEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/testApplicationContext.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@Ignore
public class TagDaoTest {

    @Autowired
    private TagDao tagDao;

    @Test
    @DatabaseSetup(value = "/testData/TagDaoTest.xml")
    @DatabaseTearDown(value = "/testData/TagDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void addTagTest() {
        try {
            Tag expectedTag = new Tag("TestTagThree");
            long tagId = tagDao.add(expectedTag);
            Tag actualTag = tagDao.get(tagId);
            assertEquals(expectedTag.getTagName(), actualTag.getTagName());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/TagDaoTest.xml")
    @DatabaseTearDown(value = "/testData/TagDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testGetAllTags() {
        List<Tag> tags = null;
        try {
            tags = tagDao.getAllTags();
        } catch (DaoException e) {
            e.printStackTrace();
        }
        assertPropertyLenientEquals("tagId", Arrays.asList(1, 2), tags);
    }

    @Test
    @DatabaseSetup(value = "/testData/TagDaoTest.xml")
    @DatabaseTearDown(value = "/testData/TagDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testEditTagName() {
        long tagId = 1L;
        try {
            Tag expectedTag = new Tag(tagId, "TestTagThree");
            tagDao.edit(expectedTag);
            Tag actualTag = tagDao.get(tagId);
            assertEquals(expectedTag.getTagName(), actualTag.getTagName());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/TagDaoTest.xml")
    @DatabaseTearDown(value = "/testData/TagDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testDeleteTag() {
        long tagId = 2L;
        try {
            tagDao.delete(tagId);
            assertNull(tagDao.get(tagId));
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DatabaseSetup(value = "/testData/TagDaoTest.xml")
    @DatabaseTearDown(value = "/testData/TagDaoTest.xml", type = DatabaseOperation.DELETE_ALL)
    public void testAddTagsToNews() {
        Tag tagOne = new Tag(1L, "TestTagOne");
        Tag tagTwo = new Tag(2L, "TestTagTwo");
        List<Tag> tags = new ArrayList<>();
        tags.add(tagOne);
        tags.add(tagTwo);
        long newsId = 2L;

        try {
            tagDao.addTagsToNews(newsId, tags);
            List<Tag> actualTags = tagDao.getNewsTagsByNewsId(newsId);
            assertEquals(2, actualTags.size());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }
}