package com.dev.newsmanagement.service;

import com.dev.newsmanagement.dao.AuthorDao;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Author;
import com.dev.newsmanagement.exception.ServiceException;
import com.dev.newsmanagement.service.impl.AuthorServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/testApplicationContext.xml")
public class AuthorServiceTest {

    @Mock
    private AuthorDao authorDao;

    @InjectMocks
    private AuthorService authorService = new AuthorServiceImpl();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddAuthor() throws Exception {
        Author author = new Author("Test");
        authorService.addAuthor(author);
        when(authorDao.add(anyObject())).thenReturn(1L);
        assertEquals(1L, authorDao.add(author));
        verify(authorDao, times(2)).add(author);
    }

    @Test
    public void testGetAllAuthors() throws DaoException, ServiceException {
        List<Author> authors = new ArrayList<>();
        when(authorDao.getAllAuthors()).thenReturn(authors);
        assertTrue(authorDao.getAllAuthors().stream().allMatch(a -> a.getAuthorName() == null));
        authorService.getAllAuthors();
        verify(authorDao, times(2)).getAllAuthors();
    }

    @Test
    public void testMakeAuthorExpired() throws ServiceException, DaoException {
        Author author = new Author(1L, "Test");
        authorService.makeAuthorExpired(author);
        verify(authorDao, times(1)).makeAuthorExpired(author.getAuthorId());
    }
}