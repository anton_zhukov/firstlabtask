package com.dev.newsmanagement.service;

import com.dev.newsmanagement.dao.CommentsDao;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Comment;
import com.dev.newsmanagement.exception.ServiceException;
import com.dev.newsmanagement.service.impl.CommentsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/testApplicationContext.xml")
public class CommentsServiceTest {

    @Mock
    private CommentsDao commentsDao;

    @InjectMocks
    private CommentsService commentsService = new CommentsServiceImpl();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddComment() throws DaoException, ServiceException {
        Comment comment = new Comment();
        commentsService.addComment(comment);
        verify(commentsDao, times(1)).add(comment);
    }

    @Test
    public void testDeleteComment() throws DaoException, ServiceException {
        long commentsId = 1L;
        commentsService.deleteComment(commentsId);
        verify(commentsDao, times(1)).delete(commentsId);
    }

    @Test
    public void testEditComment() throws DaoException, ServiceException {
        Comment comment = new Comment();
        commentsService.editCommentText(comment);
        verify(commentsDao, times(1)).edit(comment);
    }

    @Test
    public void testCountAllCommentsByNewsId() throws DaoException, ServiceException {

        when(commentsDao.countAllCommentsByNewsId(1L)).thenReturn((long) Math.random() * 10);
        commentsService.getCommentsByNewsId(1L);
        verify(commentsDao, times(1)).getCommentsByNewsId(1L);
    }
}