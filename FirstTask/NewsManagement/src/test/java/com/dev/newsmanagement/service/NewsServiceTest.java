package com.dev.newsmanagement.service;

import com.dev.newsmanagement.dao.AuthorDao;
import com.dev.newsmanagement.dao.CommentsDao;
import com.dev.newsmanagement.dao.NewsDao;
import com.dev.newsmanagement.dao.TagDao;
import com.dev.newsmanagement.entity.NewsTO;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Author;
import com.dev.newsmanagement.entity.News;
import com.dev.newsmanagement.entity.Tag;
import com.dev.newsmanagement.exception.ServiceException;
import com.dev.newsmanagement.service.impl.NewsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/testApplicationContext.xml")
public class NewsServiceTest {

    @Mock
    private NewsDao newsDao;
    @Mock
    private AuthorDao authorDao;
    @Mock
    private TagDao tagDao;
    @Mock
    private CommentsDao commentsDao;

    @InjectMocks
    private NewsService newsService = new NewsServiceImpl();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddNews() throws DaoException, ServiceException {
        News news = new News();
        String title = "Test title";
        String shortText = "Test short text";
        String fullText = "Test full text";
        news.setTitle(title);
        news.setShortText(shortText);
        news.setFullText(fullText);

        List<Tag> tags = new ArrayList<>();
        Author author = new Author(1L, "Test");
        when(newsDao.add(news)).thenReturn(1L);

        NewsTO newsTO = new NewsTO();
        newsTO.setNews(news);
        newsTO.setAuthor(author);
        newsTO.setTags(tags);
        newsService.addNews(newsTO);

        verify(newsDao, times(1)).add(news);
        verify(tagDao, times(1)).addTagsToNews(1L, tags);
        verify(authorDao, times(1)).addAuthorToNews(1L, author);
    }

    @Test
    public void testEditNews() throws DaoException, ServiceException {
        News news = new News();
        String title = "Test title";
        String shortText = "Test short text";
        String fullText = "Test full text";
        news.setTitle(title);
        news.setShortText(shortText);
        news.setFullText(fullText);

        List<Tag> tags = new ArrayList<>();
        Author author = new Author(1L, "Test");
        when(newsDao.add(news)).thenReturn(1L);

        NewsTO newsTO = new NewsTO();
        newsTO.setNews(news);
        newsTO.setAuthor(author);
        newsTO.setTags(tags);

        newsService.editNews(newsTO);
        verify(newsDao, times(1)).edit(newsTO.getNews());
    }

    @Test
    public void testDeleteNews() throws DaoException, ServiceException {
        newsService.deleteNews(1);
        verify(commentsDao, times(1)).deleteCommentsFromNews(1);
        verify(authorDao, times(1)).deleteAuthorFromNews(1);
        verify(tagDao, times(1)).deleteTagsFromNewsByIds(1);
        verify(newsDao, times(1)).deleteNews(1);
    }

    @Test
    public void testGetNewsList() throws DaoException, ServiceException {
        newsService.getNewsTOList();
        verify(newsDao, times(1)).getAllNews();
    }

    @Test
    public void testCountNews() throws DaoException, ServiceException {
        when(newsDao.countAllNews()).thenReturn(1L);
        newsService.countNews();
        verify(newsDao, times(1)).countAllNews();

    }
}