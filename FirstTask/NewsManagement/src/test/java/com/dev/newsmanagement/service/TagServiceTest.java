package com.dev.newsmanagement.service;

import com.dev.newsmanagement.dao.TagDao;
import com.dev.newsmanagement.exception.DaoException;
import com.dev.newsmanagement.entity.Tag;
import com.dev.newsmanagement.exception.ServiceException;
import com.dev.newsmanagement.service.impl.TagServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/testApplicationContext.xml")
public class TagServiceTest {

    @Mock
    private TagDao tagDao;

    @InjectMocks
    private TagService tagService = new TagServiceImpl();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddTag() throws DaoException, ServiceException {
        Tag tag = new Tag();
        tagService.addTag(tag);
        verify(tagDao, times(1)).add(tag);
    }

    @Test
    public void testGetAllTags() throws DaoException, ServiceException {
        List<Tag> tags = new ArrayList<>();
        when(tagDao.getAllTags()).thenReturn(tags);
        assertTrue(tagDao.getAllTags().stream().allMatch(t -> t.getTagName() == null));
        tagService.getAllTags();
        verify(tagDao, times(2)).getAllTags();
    }

    @Test
    public void testDeleteTag() throws DaoException, ServiceException {
        long tagId = 1L;
        tagService.deleteTag(tagId);
        verify(tagDao, times(1)).deleteTagFromAllNews(tagId);
        verify(tagDao, times(1)).delete(tagId);
    }

    @Test
    public void testEditTag() throws DaoException, ServiceException {
        Tag tag = new Tag();
        tagService.editTag(tag);
        verify(tagDao, times(1)).edit(tag);
    }
}